var getDateFormated=function(d,a){
  d=new Date(d);
  var z="";
  if(a==1){
    if(d.getDate()<10) z+="0";
    z+=d.getDate()+".";
    if(d.getMonth()<9) z+="0";
    z+=(d.getMonth()+1)+"."+d.getFullYear();
    return z;    
  }
  else if(a==2){
    if(d.getHours()<10) z+="0";
    z+=d.getHours()+":";
    if(d.getMinutes()<10) z+="0";
    z+=d.getMinutes();
    return z;    
  }else if(!a){
    return getDateFormated(d,1)+" "+getDateFormated(d,2);
  }
};