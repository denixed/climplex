/*global $*/
var mP={};
mP.init=function(){
    $('#modalPromt').modal();
    $('#mPyes').click(function(){
        if(mP.ycb) mP.ycb();
    });
    $('#mPno').click(function(){
        if(mP.ncb) mP.ncb();
    });
};
mP.setInfo=function(h,m){
    $('#mPheader').html(h||'');
    $('#mPctx').html(m||'');
};
mP.setCB=function(ycb,ncb){
    this.ycb=ycb||function(){};
    this.ncb=ncb||function(){};
};
mP.open=function(h,m,ycb,ncb){
    mP.setInfo(h,m);
    mP.setCB(ycb,ncb);
    $('#modalPromt').modal('open');
};
mP.close=function(){
    mP.setInfo();
    mP.setCB();
    $('#modalPromt').modal('close');
};