var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;

var schema = new Schema({
  name:{
      type:String,
      required:true
  },
  description:{
      type:String,
      default:''
  },
  typeOf:{
	  type:String
  }
});

module.exports = mongoose.model('factor', schema);
