var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var schema = new Schema({
	name: {
		type: String,
		required:true
	},
	eng_name: {
		type: String,
		required:true
	},
	settings:Object
});


module.exports = mongoose.model('module', schema);
