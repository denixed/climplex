var mongoose = require('../libs/mongoose');
var crypto = require('crypto');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;

var schema = new Schema({
  name: {
    type: String,
    required: true
  },
  email:{
    type:String,
    required:true,
    unique:true
  },
  secname: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  hashedPassword: {
    type: String,
    required: true
  },
  salt: {
    type: String,
    required: true
  },
  created: {
    type: Date,
    default: Date.now
  },
  tel: {
    type: String,
    required: true
  },
  hashResetPassword:String
});

schema.index({ '$**': 'text'});

schema.methods.encryptPassword = function(password) {
  return crypto.createHmac('sha256', this.salt).update(password).digest('hex');
};

schema.virtual('password')
  .set(function(password) {
    this.salt = Math.random() + '';
    this.hashedPassword = this.encryptPassword(password);
  })
  .get(function() { return null; });

schema.methods.checkPassword = function(password) {
  return this.encryptPassword(password) === this.hashedPassword;
};
module.exports = mongoose.model('user', schema);
