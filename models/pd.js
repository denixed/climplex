var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var schema = new Schema({
  factor: {
    type: ObjectID,
	required:true
  },
  created: {
    type: Date,
    default: Date.now
  },
  value: Schema.Types.Mixed
});


module.exports = mongoose.model('pd', schema);
