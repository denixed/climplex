var mongoose = require('../libs/mongoose');
var Schema = mongoose.Schema;
var ObjectID= Schema.Types.ObjectId;
var random_string=function(){
  return Math.random().toString(36).substring(2)+Math.random().toString(36).substring(2);
};
var schema = new Schema({
  auth_token: {
    type: String,
  	required:true,
  	unique:true,
  	default:random_string
  },
  auth_hash:{
    type:Number,
    default:Math.random
  },
  created: {
    type: Date,
    default: Date.now
  },
  name:{
      type:String,
      required:true
  },
  description:{
      type:String,
      default:''
  }
});

module.exports = mongoose.model('host', schema);
