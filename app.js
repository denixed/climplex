var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoStore=require('./libs/mongoStore');
var compression = require('compression');
var config = require("./libs/config");
var User = require("./models/user");
var Module = require("./models/module");
var app = express();
global.__prFolder=__dirname;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
if(!process.env.production){
  app.use(logger('dev'));
}
app.use(compression());
app.use(session({ path: '/', httpOnly: true, secret: config.session.secret, maxAge: 21600000, store:mongoStore}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
var modules;
app.use(function(req,res,next){
  res.locals.url=req.originalUrl;
  res.locals.modules=modules;
  if(req.session.user){
        User.findOne({_id:req.session.user._id},function(err,user){
            if(err) return next (err);
            res.locals.user=req.session.user=user;
            next();
        });
    }else next();
}
);
require("./routes")(app);
Module.find({},function(err,hh){
	if(err) throw err;
	if(hh.length){
		hh.forEach(function(m){
			app.use('/'+m.eng_name,require("./routes/modules/"+m.eng_name)(m.settings,m._id));
		});
	}
	modules=hh;
	app.use(function(req, res, next) {
	  var err = new Error('Не найдено');
	  err.status = 404;
	  err.stack="";
	  next(err);
	});

	app.use(function(err, req, res, next) {
	  res.locals.message = err.message;
	  res.locals.error = process.env.production ? {}: err;
	  if(err.name=='CastError') err.status=406;
	  res.status(err.status || 500);
	  if (res.req.headers['x-requested-with'] == 'XMLHttpRequest') {
		res.json(err);
	  } else {
		res.render("error");
	  }
	});
})


module.exports = app;
