var express = require('express');
var router = express.Router();
var Pd = require('../models/pd');
var Host = require('../models/host');
var Factor = require('../models/factor');


router.get('/hosts/list', function(req, res, next) {
  Host.find({},function(err,h){
     if(err) return next(err);
     res.json(h);
  });
});
router.post('/hosts/add', function(req, res, next) {
 var host= new Host(req.body);
 host.save(function(err){
   if(err) return next(err);
   res.redirect('/hosts/');
 });
});
router.post('/hosts/edit', function(req, res, next) {
 Host.findOne({_id:req.body._id},function(err,h){
    var i;
    if(err) return next(err);
    for(i in req.body){
      h[i]=req.body[i];
    }
    h.save(function(err){
      if(err) return next(err);
      res.redirect('/hosts/');
    });
  });
});
router.get('/hosts/:id', function(req, res, next) {
  Host.findOne({_id:req.params.id},function(err,h){
     if(err) return next(err);
     res.json(h);
  });
});



router.get('/factors/list', function(req, res, next) {
  Factor.find({},function(err,h){
     if(err) return next(err);
     res.json(h);
  });
});
router.post('/factors/add', function(req, res, next) {
 var factor= new Factor(req.body);
 factor.save(function(err){
   if(err) return next(err);
   res.redirect('/factors/');
 });
});
router.post('/factors/edit', function(req, res, next) {
 Factor.findOne({_id:req.body._id},function(err,h){
    var i;
    if(err) return next(err);
    for(i in req.body){
      h[i]=req.body[i];
    }
    h.save(function(err){
      if(err) return next(err);
      res.redirect('/factors/');
    });
  });
});
router.get('/factors/pd', function(req, res, next) {
  try{
	  var q= JSON.parse(req.query.json);
  }catch(e){
	err= new Error('Неправильный формат данных');
	err.status=406;
	return next(err);
  }
  Pd.find(q,function(err,h){
     if(err) return next(err);
     res.json(h);
  });
});
router.get('/factors/:id', function(req, res, next) {
  Factor.findOne({_id:req.params.id},function(err,h){
     if(err) return next(err);
     res.json(h);
  });
});
var import_pd= function(req, res, next) {
  Host.findOne({_id:req.params.id},function(err,h){
     if(err) return next(err);
	 var d;
	 if(req.body.auth_token){
		 d=req.body;
	 }else{
		 d=req.query;
	 }
     if(!h||h.auth_token!=d.auth_token){
		 console.log(h);
         err= new Error('Неправильный токен');
         err.status=403;
         return next(err);
     }
     d.auth_token=undefined;
     var i,n,pd;
     for(i in d){
		 if(d[i]){
			 pd=new Pd({
				 factor:i,
				 value:d[i]
			 });
			 pd.save(function(err){
				 if(err) console.error(err);
			 });
		 }
         
     };
	 res.send('ok');
  });
};
router.post('/pd/import/:id',import_pd);
router.get('/pd/import/:id', import_pd);
module.exports = router;
