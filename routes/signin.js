var express = require('express');
var router = express.Router();
var User = require('../models/user');

router.post('/',function(req,res,next){
  var us=req.body.email||"";
  User.findOne({'email':us},function(err,user){
    if(err) return next(err);
    if(!user) us=0;
    else {
      if(user.checkPassword(req.body.password||"")) req.session.user=res.locals.user=user;
      else us=0;
    }
    if(us){
      res.redirect("/factors/");
    }
    else{
      res.redirect("/signin#!");
    }
  });
});
router.get('/',function(req,res,next){
    if(req.session.user) res.redirect('/factors/');
    else res.render('signin');
});
module.exports = router;
