var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('factors');
});
router.get('/pd', function(req, res, next) {
  res.render('factor_pd');
});

module.exports = router;
