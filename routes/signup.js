var express = require('express');
var router = express.Router();
var User = require('../models/user');
var mailer = require("../libs/mailer");
router.post('/',function(req,res,next){
  var user=new User(req.body);
  user.save(function(err){
    if(err) return next(err);
    var text= 'Данные для входа: \n email: '+user.email+'\n password: '+req.body.password;
    mailer({to:user.email,'subject':'Данные для входа',text:text,html:text.replace(/\n/g,'<br>')});
    res.redirect('/');
  });
});
router.get('/',function(req,res,next){
    res.render('signup');
});
module.exports = router;
