var express = require('express');
var router = express.Router();
var Module=require(__prFolder+'/models/module');
var settings,module_id;
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('modules/temp');
});
router.get('/settings', function(req, res, next) {
	Module.findOne({_id:module_id},function(err,h){
		if(err) return next(err);
		res.json(h.settings);
	})
});
router.post('/settings/edit', function(req, res, next) {
    var q= req.body;
	Module.findOne({_id:module_id},function(err,h){
		if(err) return next(err);
		console.log(h);
		for(i in q){
		  h.settings[i]=q[i];
		}
		h.markModified('settings');
		h.save(function(err){
		  if(err) return next(err);
		  res.redirect('/'+h.eng_name);
		});
	})
});

module.exports = function(set,id){
	settings=set;
	module_id=id;
	return router;
};
