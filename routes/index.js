var E=require("../models/error");
module.exports = function(app){
  app.use('/', require("./home"));
  app.use('/signin', require("./signin")); 
  app.use('/signout', require("./signout")); 
  app.use('/signup', require("./signup")); 
  app.use('/hosts', require("./hosts")); 
  app.use('/factors', require("./factors")); 
  app.use('/api', require("./api")); 

  app.post('/error/log',function(req,res,next){
    res.sendStatus(200);
    var err;
    err=JSON.parse(req.body.json);
    err.user=req.session.user;
    console.log(err);
    (new E(err)).save(function(err){
      if(err) throw err;
    });
  });
};