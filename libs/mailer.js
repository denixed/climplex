var nodemailer = require('nodemailer');
var config =require("./config");
var transporter = nodemailer.createTransport(config.mail);
var started=false;
transporter.verify(function(error, success) {
   if (error) {
        console.log(error);
   } else {
        console.log('Server is ready to take our messages');
		started=1;
   }
});

var mailOptions = config.mailOptions;
module.exports=function(data,cb){
	if(!started) return cb?cb():'';
    data.from=mailOptions.from;
    transporter.sendMail(data, function(err) {
        if (err) {
            return cb(err);
        }
        cb();
    });
};
